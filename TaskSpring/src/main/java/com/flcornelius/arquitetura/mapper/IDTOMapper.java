package com.flcornelius.arquitetura.mapper;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.flcornelius.arquitetura.dto.IDTO;
import com.flcornelius.arquitetura.entities.IEntity;
import com.flcornelius.arquitetura.entities.IIdentifier;

public interface IDTOMapper<E extends IEntity, D extends IDTO> {

	D convertEntityToDTO(E entity);

	IIdentifier<Long> convertDTOToEntity(D idto);

	List<E> convertDTOListToEntityList(List<D> dtoList);

	List<D> converterListToDTOList(List<E> entityList);

	Page<D> clientePageToClienteDTOPage(Pageable pageable, Page<E> taskPage);

}
