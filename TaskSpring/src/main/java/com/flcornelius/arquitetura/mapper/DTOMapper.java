package com.flcornelius.arquitetura.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.flcornelius.arquitetura.dto.IDTO;
import com.flcornelius.arquitetura.entities.IEntity;

public class DTOMapper<E extends IEntity, D extends IDTO> implements IDTOMapper<E, D> {

	private Class<E> classEntity;

	private Class<D> classDto;

	private ModelMapper modelMapper = new ModelMapper();

	public DTOMapper(Class<E> classEntity, Class<D> classDto) {
		this.classEntity = classEntity;
		this.classDto = classDto;
	}

	@Override
	public D convertEntityToDTO(E entity) {
		return modelMapper.map(entity, classDto);
	}

	@Override
	public E convertDTOToEntity(D idto) {
		return modelMapper.map(idto, classEntity);
	}

	@Override
	public List<E> convertDTOListToEntityList(List<D> dtoList) {
		return dtoList.stream().filter(Objects::nonNull).map(this::convertDTOToEntity).collect(Collectors.toList());
	}

	@Override
	public List<D> converterListToDTOList(List<E> entityList) {
		return entityList.stream().filter(Objects::nonNull).map(this::convertEntityToDTO).collect(Collectors.toList());
	}

	@Override
	public Page<D> clientePageToClienteDTOPage(Pageable pageable, Page<E> taskPage) {
		int totalElements = (int) taskPage.getTotalElements();
		return new PageImpl<D>(taskPage.stream().map(this::convertEntityToDTO).collect(Collectors.toList()), pageable,
				totalElements);
	}

}
