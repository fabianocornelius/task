package com.flcornelius.arquitetura.entities;

import java.io.Serializable;

public interface IIdentifier<I> extends Serializable {
	I getId();
}
