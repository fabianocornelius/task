package com.flcornelius.arquitetura.controller;

import java.util.List;

import com.flcornelius.arquitetura.dto.IDTO;

public interface ICRUDController<T extends IDTO> {

	T save(T entity);
	
	void delete(Long id);

	List<T> findAll();

	T findById(Long id);
}
