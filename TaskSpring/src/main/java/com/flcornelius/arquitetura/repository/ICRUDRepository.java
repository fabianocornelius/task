package com.flcornelius.arquitetura.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.flcornelius.arquitetura.entities.IEntity;

public interface ICRUDRepository<T extends IEntity> extends JpaRepository<T, Long> {

}
