package com.flcornelius.arquitetura.dto;

import com.flcornelius.arquitetura.entities.IIdentifier;

public interface IDTO extends IIdentifier<Long> {
}
