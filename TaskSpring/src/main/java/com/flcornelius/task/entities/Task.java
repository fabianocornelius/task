package com.flcornelius.task.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.flcornelius.arquitetura.entities.IEntity;

@Entity
public class Task implements IEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long idTask;

	private String descricao;

	private String conteudo;

	public Task() {
	}

	public Long getIdTask() {
		return idTask;
	}

	public void setIdTask(Long idTask) {
		this.idTask = idTask;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	@Override
	public Long getId() {
		return getIdTask();
	}

}
