package com.flcornelius.task.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.flcornelius.arquitetura.dto.IDTO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TaskDTO implements IDTO {

	private static final long serialVersionUID = 1L;

	private Long idTask;

	private String descricao;

	private String conteudo;

	public TaskDTO() {
	}

	public Long getIdTask() {
		return idTask;
	}

	public void setIdTask(Long idTask) {
		this.idTask = idTask;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	@Override
	public Long getId() {
		return idTask;
	}

}
