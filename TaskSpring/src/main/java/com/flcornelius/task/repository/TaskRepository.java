package com.flcornelius.task.repository;

import org.springframework.stereotype.Repository;

import com.flcornelius.arquitetura.repository.ICRUDRepository;
import com.flcornelius.task.entities.Task;

@Repository
public interface TaskRepository extends ICRUDRepository<Task> {

}
