package com.flcornelius.task.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flcornelius.arquitetura.controller.ICRUDController;
import com.flcornelius.task.dto.TaskDTO;
import com.flcornelius.task.entities.Task;
import com.flcornelius.task.repository.TaskRepository;
import com.flcornelius.task.service.TaskMapper;

@RestController
@RequestMapping("/api/tasks")
public class TaskController implements ICRUDController<TaskDTO> {

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private TaskMapper taskMapper;

	@Override
	@PostMapping("/save")
	public TaskDTO save(@RequestBody TaskDTO dto) {
		Task entity = taskMapper.convertDTOToEntity(dto);
		return taskMapper.convertEntityToDTO(taskRepository.save(entity));
	}

	@Override
	@GetMapping
	public List<TaskDTO> findAll() {
		return taskMapper.converterListToDTOList(taskRepository.findAll());
	}

	@Override
	@GetMapping("/{id}")
	public TaskDTO findById(@PathVariable("id") Long id) {
		Optional<Task> entityOptional = taskRepository.findById(id);
		if (entityOptional.isPresent()) {
			return taskMapper.convertEntityToDTO(entityOptional.get());
		}
		return null;
	}

	@Override
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		taskRepository.deleteById(id);
	}

}
