package com.flcornelius.task.service;

import org.springframework.stereotype.Service;

import com.flcornelius.arquitetura.mapper.DTOMapper;
import com.flcornelius.task.dto.TaskDTO;
import com.flcornelius.task.entities.Task;

@Service
public class TaskMapper extends DTOMapper<Task, TaskDTO> {

	public TaskMapper() {
		super(Task.class, TaskDTO.class);
	}
}