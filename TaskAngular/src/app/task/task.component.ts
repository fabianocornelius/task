﻿import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Task } from "../_models";

import { TaskService } from '../_services';

@Component({ templateUrl: 'task.component.html' })
export class TaskComponent implements OnInit {
    registerForm: FormGroup;
    submitted = false;
    tasks: Task[];

    constructor(
        private formBuilder: FormBuilder,
        private taskService: TaskService) { }

    ngOnInit() {
        this.novoRegistro();
        this.atualizarList();
    }

    onTaskDrop(task: any) {
        this.deleteTask(task.dragData);
    }

    get f() { return this.registerForm.controls; }

    onSubmit() {

        this.submitted = true;
        if (this.registerForm.invalid) {
            return;
        }

        let task = new Task(this.registerForm.value.descricao, this.registerForm.value.conteudo);

        this.taskService.register(task);

        this.atualizarList();
        this.novoRegistro();

    }

    deleteTask(task: Task): void {
        this.taskService.delete(task.idTask)
            .subscribe(data => {
                this.tasks = this.tasks.filter(u => u !== task);
            })
    };

    atualizarList() {
        this.taskService.getAll().subscribe(data => {
            this.tasks = data;
        });
    }

    novoRegistro() {
        this.registerForm = this.formBuilder.group({
            descricao: ['', Validators.required],
            conteudo: ['', Validators.required]
        });
    }
}
