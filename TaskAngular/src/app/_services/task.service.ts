import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Task } from '../_models';

@Injectable()
export class TaskService {

    constructor(private httpClient: HttpClient, private http: Http) { }

    getAll() {
        return this.httpClient.get<Task[]>(`${environment.apiUrl}/tasks`);
    }

    getById(id: number) {
        return this.httpClient.get<Task>(`${environment.apiUrl}/tasks/` + id);
    }

    register(task: Task) {
        let taskHeaders = new Headers({ 'Content-Type': 'application/json' });
        return this.http.post(`${environment.apiUrl}/tasks/save`, JSON.stringify(task), { headers: taskHeaders })
            .toPromise()
            .then(response => response.json().results as Task[])
            .catch(this.handleError);;
    }

    update(task: Task) {
        return this.httpClient.put(`${environment.apiUrl}/tasks/` + task.idTask, task);
    }

    delete(id: number) {
        const url = `${environment.apiUrl}/tasks/${id}`;
        return this.httpClient.delete(url);
    }

    private handleError(error: any): Promise<Array<any>> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}