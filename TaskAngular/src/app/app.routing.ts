﻿import { Routes, RouterModule } from '@angular/router';

import { TaskComponent } from './task';

const appRoutes: Routes = [
    { path: '', component: TaskComponent },
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);