﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { NgDragDropModule } from 'ng-drag-drop';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { TaskService } from './_services';

import { TaskComponent } from './task';
import { FooterComponent } from './ui/footer';
import { LayoutComponent } from './ui/layout';
import { HeaderComponent } from './ui/header';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        HttpModule,
        NgDragDropModule.forRoot(),
        routing
    ],
    declarations: [
        AppComponent,
        TaskComponent,
        FooterComponent,
        LayoutComponent,
        HeaderComponent
    ],
    providers: [
        TaskService
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }